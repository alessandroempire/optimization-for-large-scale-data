#The diet problem

#clase_diet_problem

%%writefile diet.py

from __future__ import division
from pyomo.environ import *

model = ConcreteModel()

model.x_b = Var(domain=NonNegativeReals) #broccoli
model.x_m = Var(domain=NonNegativeReals) #milk
model.x_o = Var(domain=NonNegativeReals) #oranges

model.OBJ = Objective(expr = 0.381*model.x_b+0.1*model.x_m+0.272*model.x_o,sense=minimize)

model.c1 = Constraint(expr = 47*model.x_b+276*model.x_m+40*model.x_o >= 1000)
model.c2 = Constraint(expr = 89.2*model.x_b+53.2*model.x_o >= 90)
model.c3 = Constraint(expr = 91*model.x_b+87*model.x_m+87*model.x_o >= 3700)

!pyomo solve diet.py --solver=glpk

!type results.yml
