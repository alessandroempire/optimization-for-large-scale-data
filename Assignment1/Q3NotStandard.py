
# Google Adwords Problem

from __future__ import division 
from pyomo.environ import *

model = AbstractModel()

#First we define the sets (values) i,j can take
model.I = Set()  					#Set of Companies
model.J = Set()  					#Set of queries

#Define the variables we will use
model.k = Param(model.I, model.J)  # Average revenue of each company per query
model.b = Param(model.I)           # Budget per company
model.e = Param(model.J)           # Estimated request per query

# the next line declares a variable indexed by the set I and set J
model.x = Var(model.I, model.J, domain=NonNegativeReals)


#Objective Function
def obj_expression(model):
	return summation(model.k, model.x)

model.OBJ = Objective(rule=obj_expression, sense=maximize)


# Rule over the budget
def constraint_rule_budget(model, i):
	return sum(model.k[i,j] * model.x[i,j] for j in model.J) <= model.b[i]

# the next line creates one constraint for each member of the set model.I
model.budget_Constraint = Constraint(model.I, rule=constraint_rule_budget)


#Rule over the requests
def constraint_rule_requests(model, j):
	return sum(model.x[i,j] for i in model.I) <= model.e[j]

# the next line creates one constraint for each member of the set model.J
model.request_Constraint = Constraint(model.J, rule=constraint_rule_requests)

