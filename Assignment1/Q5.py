# Google Adwords Problem
# pyomo solve Q5.py Q5.dat --solver=glpk

from __future__ import division 
from pyomo.environ import *

model = AbstractModel()

#First we define the sets (values) i,j can take
model.I = Set()  					#Set of Companies
model.J = Set()  					#Set of queries

#Define the variables we will use
model.k = Param(model.I, model.J)  # Average revenue of each company per query
model.b = Param(model.I)           # Budget per company
model.e = Param(model.J)           # Estimated request per query

# the next line declares a variable indexed by the set I and set J
model.y = Var(model.I, domain=NonNegativeReals)
model.v = Var(model.J, domain=NonNegativeReals)

#Objective Function
def obj_expression(model):
	return (summation(model.b, model.y) + summation(model.e, model.v))

model.OBJ = Objective(rule=obj_expression, sense=minimize)


# Rule over the budget
def constraint_dual(model, i, j):
	return model.k[i,j] * model.y[i] + model.v[j] >= model.k[i,j]

# the next line creates one constraint for each member of the set model.I, model.J
model.constraint_dual = Constraint(model.I, model.J, rule=constraint_dual)
