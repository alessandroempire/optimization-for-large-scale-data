#The transportation problem

# %%writefile transportation-problem.py

from __future__ import division 
from pyomo.environ import *

model = AbstractModel()

model.n = Param(within =NonNegativeIntegers) #number of problems
model.m = Param(within =NonNegativeIntegers) #number of markets

model.I = RangeSet(1,model.n) 				#factories
model.J = RangeSet(1,model.m) 				#markets

model.c = Param(model.I,model.J) #transportation cost
model.b = Param(model.J) 		 #demand for each market
model.a = Param(model.I) 		 #max capacity factory

model.x = Var(model.I, model.J, domain=NonNegativeReals)

def obj_expression(model):
    return 90*summation(model.c,model.x)

model.OBJ = Objective(rule=obj_expression, sense=maximize)

def b_constraint(model,j):
    return sum(model.x[i,j] for i in model.I) >= model.b[j]

model.dem_cons = Constraint(model.J, rule=b_constraint)


###########################################################
def a_constraint(model,i):
    return sum(model.x[i,j] for j in model.J) <= model.a[i]

model.prod_cons = Constraint(model.I, rule=a_constraint)



#run the model
# !pyomo solve transportation_gen.py transportation_gen.dat --solver=glpk #for sensitivity add "--solver-suffix=dual"

#run results
# !type results.yml